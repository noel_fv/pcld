package com.bbva.pcld.dto.documento.common;

import com.bbva.apx.dto.AbstractDTO;

public class FieldDTO extends AbstractDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -508453581548762924L;

	private String name;
	private String type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Field [name=" + name + ", type=" + type + "]";
	}

}
