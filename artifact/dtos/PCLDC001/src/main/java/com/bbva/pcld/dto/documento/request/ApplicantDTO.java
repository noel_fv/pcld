package com.bbva.pcld.dto.documento.request;

import java.util.List;

import com.bbva.apx.dto.AbstractDTO;
import com.bbva.pcld.dto.documento.common.ParameterDTO;

public class ApplicantDTO extends AbstractDTO {
	private static final long serialVersionUID = -2113576143477326266L;

	private String idType;
	private String id;
	private List<ParameterDTO> parameters;

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<ParameterDTO> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParameterDTO> parameters) {
		this.parameters = parameters;
	}

	@Override
	public String toString() {
		return "ApplicantDTO [idType=" + idType + ", id=" + id + ", parameters=" + parameters + "]";
	}

}
