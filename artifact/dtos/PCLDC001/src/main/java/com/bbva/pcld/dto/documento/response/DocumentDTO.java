package com.bbva.pcld.dto.documento.response;

import java.util.List;

import com.bbva.apx.dto.AbstractDTO;
import com.bbva.pcld.dto.documento.common.ParameterDTO;

public class DocumentDTO extends AbstractDTO {
	private static final long serialVersionUID = -4813395077488614074L;

	private String code;
	private String name;
	private String description;
	private Boolean required;
	private String id;
	private List<ParameterDTO> parameters;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<ParameterDTO> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParameterDTO> parameters) {
		this.parameters = parameters;
	}

	@Override
	public String toString() {
		return "DocumentDTO [code=" + code + ", name=" + name + ", description=" + description + ", required="
				+ required + ", id=" + id + ", parameters=" + parameters + "]";
	}

}
