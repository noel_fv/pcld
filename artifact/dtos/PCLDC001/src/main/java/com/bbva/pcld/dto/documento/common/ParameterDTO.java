package com.bbva.pcld.dto.documento.common;

import com.bbva.apx.dto.AbstractDTO;

public class ParameterDTO extends AbstractDTO {
	private static final long serialVersionUID = -7133295245569627863L;

	private FieldDTO field;
	private String value;

	public FieldDTO getField() {
		return field;
	}

	public void setField(FieldDTO field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ParametersDTO [field=" + field + ", value=" + value + "]";
	}

}
