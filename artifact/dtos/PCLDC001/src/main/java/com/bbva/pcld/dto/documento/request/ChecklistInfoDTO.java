package com.bbva.pcld.dto.documento.request;

import com.bbva.apx.dto.AbstractDTO;

public class ChecklistInfoDTO extends AbstractDTO {
	private static final long serialVersionUID = 8396614951376960252L;

	private String date;
	private Boolean reusability;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Boolean getReusability() {
		return reusability;
	}

	public void setReusability(Boolean reusability) {
		this.reusability = reusability;
	}

	@Override
	public String toString() {
		return "ChecklistInfoDTO [date=" + date + ", reusability=" + reusability + "]";
	}

}
