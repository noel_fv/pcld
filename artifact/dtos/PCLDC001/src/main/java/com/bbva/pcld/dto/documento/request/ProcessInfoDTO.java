package com.bbva.pcld.dto.documento.request;

import java.util.List;

import com.bbva.apx.dto.AbstractDTO;
import com.bbva.pcld.dto.documento.common.ParameterDTO;

public class ProcessInfoDTO extends AbstractDTO {
	private static final long serialVersionUID = 6510226696263862403L;

	private String code;
	private String stage;
	private String id;
	private List<ParameterDTO> parameters;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<ParameterDTO> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParameterDTO> parameters) {
		this.parameters = parameters;
	}

	@Override
	public String toString() {
		return "ProcessInfoDTO [code=" + code + ", stage=" + stage + ", id=" + id + ", parameters=" + parameters + "]";
	}

}
