package com.bbva.pcld.lib.r002;

import java.util.Map;

import com.bbva.pcld.dto.documento.request.ChecklistInfoDTO;

public interface PCLDR002 {

	 Map<String, String> execute(ChecklistInfoDTO checklistInfoDTO);

}
