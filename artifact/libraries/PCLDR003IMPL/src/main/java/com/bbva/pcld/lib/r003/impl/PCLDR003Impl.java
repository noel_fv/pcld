package com.bbva.pcld.lib.r003.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PCLDR003Impl extends PCLDR003Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger("PCLDR003Impl");
	private static final String TRACE_PCLDR003_IMPL = "[PCLD][PCLDR003Impl] - %s";

	@Override
	public void execute() {
		LOGGER.info(String.format(TRACE_PCLDR003_IMPL, "execute START"));
		List<Map<String, Object>> mapOut = paomR002.executeGetCatalogoInformation();
		LOGGER.info(String.format(TRACE_PCLDR003_IMPL, "execute END"));

	}
}
