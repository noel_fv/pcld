package com.bbva.pcld.lib.r003.impl;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.library.AbstractLibrary;
import com.bbva.paom.lib.r002.PAOMR002;
import com.bbva.pcld.lib.r003.PCLDR003;

public abstract class PCLDR003Abstract extends AbstractLibrary implements PCLDR003 {

	protected ApplicationConfigurationService applicationConfigurationService;

	protected PAOMR002 paomR002;


	/**
	* @param applicationConfigurationService the this.applicationConfigurationService to set
	*/
	public void setApplicationConfigurationService(ApplicationConfigurationService applicationConfigurationService) {
		this.applicationConfigurationService = applicationConfigurationService;
	}

	/**
	* @param paomR002 the this.paomR002 to set
	*/
	public void setPaomR002(PAOMR002 paomR002) {
		this.paomR002 = paomR002;
	}

}