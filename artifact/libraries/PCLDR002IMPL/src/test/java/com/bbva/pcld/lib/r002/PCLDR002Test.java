package com.bbva.pcld.lib.r002;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;

import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import com.bbva.paom.lib.r001.PAOMR001;
import com.bbva.pcld.dto.documento.request.ChecklistInfoDTO;
import com.bbva.pcld.lib.r002.impl.PCLDR002Impl;

//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(Parameterized.class)
@ContextConfiguration(locations = { "classpath:/META-INF/spring/PCLDR002-app.xml",
		"classpath:/META-INF/spring/PCLDR002-app-test.xml", "classpath:/META-INF/spring/PCLDR002-arc.xml",
		"classpath:/META-INF/spring/PCLDR002-arc-test.xml" })
public class PCLDR002Test {

	@InjectMocks
	PCLDR002Impl pcldR002Impl;

	@Mock
	PAOMR001 paomR001;

	private String inputDate;

	@Parameters
	public static Collection<String[]> allParameters() {
		String input[][] = { { "20200206" }, { "20200212" } };
		return Arrays.asList(input);
	}

	public PCLDR002Test(String inputDate) {
		this.inputDate = inputDate;
	}

	@Before
	public void setUp() throws Exception {
		ThreadContext.set(new Context());
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void executeTestWithData() {
		ChecklistInfoDTO checklistInfoDTO = new ChecklistInfoDTO();
		checklistInfoDTO.setDate(inputDate);
		Mockito.when(paomR001.executeGetVersionInformation()).thenReturn(setOutput());
		assertNotNull(pcldR002Impl.execute(checklistInfoDTO));
	}

	@Test
	public void executeTestWithEmptyResult() {
		ChecklistInfoDTO checklistInfoDTO = new ChecklistInfoDTO();
		checklistInfoDTO.setDate("20200206");
		Mockito.when(paomR001.executeGetVersionInformation()).thenReturn(Collections.emptyList());
		assertTrue(pcldR002Impl.execute(checklistInfoDTO).isEmpty());
	}

	private List<Map<String, Object>> setOutput() {
		List<Map<String, Object>> mapOut = new ArrayList<Map<String, Object>>();
		Map<String, Object> output = new HashMap<String, Object>();
		output.put("RULE_NAME", "valor1");
		output.put("VERSION_NAME", "1");
		output.put("START_DATE", new DateTime("2020-02-02").toDate());
		output.put("END_DATE", new DateTime("2020-02-08").toDate());
		mapOut.add(output);
		return mapOut;
	}

}
