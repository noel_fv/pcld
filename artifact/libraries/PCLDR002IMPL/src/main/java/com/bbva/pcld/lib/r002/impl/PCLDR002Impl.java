package com.bbva.pcld.lib.r002.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bbva.pcld.dto.documento.common.Constants;
import com.bbva.pcld.dto.documento.request.ChecklistInfoDTO;

public class PCLDR002Impl extends PCLDR002Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger("PCLDR002Impl");
	private static final String TRACE_PCLDR002_IMPL = "[PCLD][PCLDR002Impl] - %s";
	private static final String RULE_NAME = "RULE_NAME";
	private static final String START_DATE = "START_DATE";
	private static final String END_DATE = "END_DATE";
	private static final String VERSION_NAME = "VERSION_NAME";
	private static final String NAMESPACE_NAME = "NAMESPACE_NAME";

	@Override
	public Map<String, String> execute(ChecklistInfoDTO checklistInfoDTO) {
		LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "execute START"));
		LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "getting data from PAOMR001"));
		List<Map<String, Object>> mapOut = paomR001.executeGetVersionInformation();
		if (CollectionUtils.isEmpty(mapOut)) {
			LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "There is no data in PAOMR001"));
			return Collections.emptyMap();
		}
		String requestDate = checklistInfoDTO.getDate();
		LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "request date: " + requestDate));
		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(Constants.DATE_FORMAT);
		DateTime dateTime = dateTimeFormatter.parseDateTime(requestDate);

		List<String> rulesNameList = new ArrayList<>();
		List<Date> startDateList = new ArrayList<>();
		List<Date> endDateList = new ArrayList<>();
		List<String> versionList = new ArrayList<String>();
		List<String> namespaceList = new ArrayList<String>();

		Map<String, String> result = new HashMap<String, String>();

		mapOut.forEach(map -> {
			rulesNameList.add((String) map.get(RULE_NAME));
			startDateList.add((Date) map.get(START_DATE));
			endDateList.add((Date) map.get(END_DATE));
			versionList.add((String) map.get(VERSION_NAME));
			namespaceList.add((String) map.get(NAMESPACE_NAME));
		});

		for (int i = 0; i < mapOut.size(); i++) {
			LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "startDateList.get(i):" + startDateList.get(i)));
			LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "dateTime.toDate(): " + dateTime.toDate()));
			LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "endDateList.get(i): " + endDateList.get(i)));

			if (startDateList.get(i).compareTo(dateTime.toDate())
					* dateTime.toDate().compareTo(endDateList.get(i)) > 0) {
				LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "success"));
				result.put(RULE_NAME, rulesNameList.get(i));
				result.put(VERSION_NAME, versionList.get(i));
				result.put(NAMESPACE_NAME, namespaceList.get(i));
				break;
			}
		}
		
		LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "rulesName: " + result.get(RULE_NAME)));
		LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "versionRules: " + result.get(VERSION_NAME)));
		LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "namespace: " + result.get(NAMESPACE_NAME)));
		LOGGER.info(String.format(TRACE_PCLDR002_IMPL, "execute END"));

		return result;
	}
}
