package com.bbva.pcld.lib.r002.impl;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.library.AbstractLibrary;
import com.bbva.paom.lib.r001.PAOMR001;
import com.bbva.pcld.lib.r002.PCLDR002;

public abstract class PCLDR002Abstract extends AbstractLibrary implements PCLDR002 {

	protected ApplicationConfigurationService applicationConfigurationService;

	protected PAOMR001 paomR001;


	/**
	* @param applicationConfigurationService the this.applicationConfigurationService to set
	*/
	public void setApplicationConfigurationService(ApplicationConfigurationService applicationConfigurationService) {
		this.applicationConfigurationService = applicationConfigurationService;
	}

	/**
	* @param paomR001 the this.paomR001 to set
	*/
	public void setPaomR001(PAOMR001 paomR001) {
		this.paomR001 = paomR001;
	}

}