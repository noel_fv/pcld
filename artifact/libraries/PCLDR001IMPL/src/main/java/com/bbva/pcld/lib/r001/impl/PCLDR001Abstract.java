package com.bbva.pcld.lib.r001.impl;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.library.AbstractLibrary;
import com.bbva.pcld.lib.r001.PCLDR001;
import com.bbva.pcld.lib.r002.PCLDR002;
import com.bbva.pcld.lib.r003.PCLDR003;
import com.bbva.pcld.lib.r004.PCLDR004;
import com.bbva.pcld.lib.r005.PCLDR005;
import com.bbva.pcld.lib.r006.PCLDR006;

public abstract class PCLDR001Abstract extends AbstractLibrary implements PCLDR001 {

	protected ApplicationConfigurationService applicationConfigurationService;

	protected PCLDR002 pcldR002;

	protected PCLDR003 pcldR003;

	protected PCLDR004 pcldR004;

	protected PCLDR005 pcldR005;

	protected PCLDR006 pcldR006;


	/**
	* @param applicationConfigurationService the this.applicationConfigurationService to set
	*/
	public void setApplicationConfigurationService(ApplicationConfigurationService applicationConfigurationService) {
		this.applicationConfigurationService = applicationConfigurationService;
	}

	/**
	* @param pcldR002 the this.pcldR002 to set
	*/
	public void setPcldR002(PCLDR002 pcldR002) {
		this.pcldR002 = pcldR002;
	}

	/**
	* @param pcldR003 the this.pcldR003 to set
	*/
	public void setPcldR003(PCLDR003 pcldR003) {
		this.pcldR003 = pcldR003;
	}

	/**
	* @param pcldR004 the this.pcldR004 to set
	*/
	public void setPcldR004(PCLDR004 pcldR004) {
		this.pcldR004 = pcldR004;
	}

	/**
	* @param pcldR005 the this.pcldR005 to set
	*/
	public void setPcldR005(PCLDR005 pcldR005) {
		this.pcldR005 = pcldR005;
	}

	/**
	* @param pcldR006 the this.pcldR006 to set
	*/
	public void setPcldR006(PCLDR006 pcldR006) {
		this.pcldR006 = pcldR006;
	}

}