package com.bbva.pcld.lib.r001.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.pcld.dto.documento.request.ApplicantDTO;
import com.bbva.pcld.dto.documento.request.ChecklistInfoDTO;
import com.bbva.pcld.dto.documento.request.ProcessInfoDTO;

public class PCLDR001Impl extends PCLDR001Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger("PCLDR001Impl");

	@Override
	public boolean executeGetDocuments(ChecklistInfoDTO checklistInfoDTO, ApplicantDTO applicantDTO,
			ProcessInfoDTO processInfoDTO) {

		LOGGER.info("[PCLD][PCLDR001Impl] executeGetDocuments() START");
		pcldR002.execute(checklistInfoDTO);
		pcldR003.execute();
		pcldR004.execute(applicantDTO, processInfoDTO);
		LOGGER.info("[PCLD][PCLDR001Impl] executeGetDocuments() END");
		return false;
	}
}
