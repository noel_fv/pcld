package com.bbva.pcld.lib.r001;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import com.bbva.pcld.dto.documento.request.ProcessInfoDTO;
import com.bbva.pcld.dto.documento.request.ApplicantDTO;
import com.bbva.pcld.dto.documento.request.ChecklistInfoDTO;

import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/META-INF/spring/PCLDR001-app.xml",
		"classpath:/META-INF/spring/PCLDR001-app-test.xml", "classpath:/META-INF/spring/PCLDR001-arc.xml",
		"classpath:/META-INF/spring/PCLDR001-arc-test.xml" })
public class PCLDR001Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCLDR001Test.class);

	@Resource(name = "pcldR001")
	private PCLDR001 pcldR001;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		ThreadContext.set(new Context());
		getObjectIntrospection();
	}

	private Object getObjectIntrospection() throws Exception {
		Object result = this.pcldR001;
		if (this.pcldR001 instanceof Advised) {
			Advised advised = (Advised) this.pcldR001;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}

	@Test
	public void executeTest() {
		LOGGER.info("Executing the test...");
//		SolicitudDTO solicitudDTO = new SolicitudDTO();
//		solicitudDTO.setFecha("fecha");
//		SolicitanteDTO solicitanteDTO = new SolicitanteDTO();
//		solicitanteDTO.setNroIdentificador("1");
//		ProcesoDTO procesoDTO = null;
//		pcldR001.executeGetVersion(solicitudDTO, solicitanteDTO, procesoDTO);
		;
	}

}
