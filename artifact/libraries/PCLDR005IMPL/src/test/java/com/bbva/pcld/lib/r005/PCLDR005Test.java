package com.bbva.pcld.lib.r005;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/PCLDR005-app.xml",
		"classpath:/META-INF/spring/PCLDR005-app-test.xml",
		"classpath:/META-INF/spring/PCLDR005-arc.xml",
		"classpath:/META-INF/spring/PCLDR005-arc-test.xml" })
public class PCLDR005Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCLDR005Test.class);

	@Resource(name = "pcldR005")
	private PCLDR005 pcldR005;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		ThreadContext.set(new Context());
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.pcldR005;
		if(this.pcldR005 instanceof Advised){
			Advised advised = (Advised) this.pcldR005;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}
	
	@Test
	public void executeTest(){
		LOGGER.info("Executing the test...");
		pcldR005.execute();
	}
	
}
