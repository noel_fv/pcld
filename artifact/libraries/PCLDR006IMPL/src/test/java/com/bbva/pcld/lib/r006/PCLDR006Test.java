package com.bbva.pcld.lib.r006;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/PCLDR006-app.xml",
		"classpath:/META-INF/spring/PCLDR006-app-test.xml",
		"classpath:/META-INF/spring/PCLDR006-arc.xml",
		"classpath:/META-INF/spring/PCLDR006-arc-test.xml" })
public class PCLDR006Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCLDR006Test.class);

	@Resource(name = "pcldR006")
	private PCLDR006 pcldR006;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		ThreadContext.set(new Context());
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.pcldR006;
		if(this.pcldR006 instanceof Advised){
			Advised advised = (Advised) this.pcldR006;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}
	
	@Test
	public void executeTest(){
		LOGGER.info("Executing the test...");
		pcldR006.execute();
	}
	
}
