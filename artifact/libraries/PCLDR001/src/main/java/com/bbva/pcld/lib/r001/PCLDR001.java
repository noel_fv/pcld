package com.bbva.pcld.lib.r001;

import com.bbva.pcld.dto.documento.request.ProcessInfoDTO;
import com.bbva.pcld.dto.documento.request.ApplicantDTO;
import com.bbva.pcld.dto.documento.request.ChecklistInfoDTO;

public interface PCLDR001 {

	boolean executeGetDocuments(ChecklistInfoDTO checklistInfoDTO, ApplicantDTO applicantDTO,
			ProcessInfoDTO processInfoDTO);

}
