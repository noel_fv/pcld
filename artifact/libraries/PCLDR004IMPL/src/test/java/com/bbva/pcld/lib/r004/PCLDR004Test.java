package com.bbva.pcld.lib.r004;

import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/META-INF/spring/PCLDR004-app.xml",
		"classpath:/META-INF/spring/PCLDR004-app-test.xml", "classpath:/META-INF/spring/PCLDR004-arc.xml",
		"classpath:/META-INF/spring/PCLDR004-arc-test.xml" })
public class PCLDR004Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(PCLDR004Test.class);

	@Resource(name = "pcldR004")
	private PCLDR004 pcldR004;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		ThreadContext.set(new Context());
		getObjectIntrospection();
	}

	private Object getObjectIntrospection() throws Exception {
		Object result = this.pcldR004;
		if (this.pcldR004 instanceof Advised) {
			Advised advised = (Advised) this.pcldR004;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}

	@Test
	public void executeTest() {
		LOGGER.info("Executing the test...");
		assertTrue(true);
//		ProcessInfoDTO processInfoDTO=null;
//		ApplicantDTO applicantDTO=null;
//		pcldR004.execute(applicantDTO, processInfoDTO);
	}

}
