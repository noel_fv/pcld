package com.bbva.pcld.lib.r004.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.pcld.dto.documento.request.ApplicantDTO;
import com.bbva.pcld.dto.documento.request.ProcessInfoDTO;

public class PCLDR004Impl extends PCLDR004Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger("PCLDR004Impl");
	private static final String TRACE_PCLDR004_IMPL = "[PCLD][PCLDR004Impl] - %s";

	@Override
	public void execute(ApplicantDTO applicantDTO, ProcessInfoDTO processInfoDTO) {
		LOGGER.info(String.format(TRACE_PCLDR004_IMPL, "execute START"));

		List<Map<String, Object>> mapIn = Collections.emptyList();
		// ID AUTOGENERADO
//		ID_peticion						applicantDTO.getId();
//		CANAL							this.getRequestHeader().getHeaderParameter(RequestHeaderParamsName.BRANCHCODE);
//		proceso_codigo					processInfoDTO.getCode();		
//		proceso_tarea					processInfoDTO.getStage();
//		proceso_identificador			processInfoDTO.getId();
//		solicitante_tipoIdentificador	applicantDTO.getIdType();
//		solicitante_nroIdentificador	applicantDTO.getId();
		paomR003.execute(mapIn);
		LOGGER.info(String.format(TRACE_PCLDR004_IMPL, "execute END"));


	}
}
