package com.bbva.pcld.lib.r004.impl;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.library.AbstractLibrary;
import com.bbva.paom.lib.r003.PAOMR003;
import com.bbva.pcld.lib.r004.PCLDR004;

public abstract class PCLDR004Abstract extends AbstractLibrary implements PCLDR004 {

	protected ApplicationConfigurationService applicationConfigurationService;

	protected PAOMR003 paomR003;


	/**
	* @param applicationConfigurationService the this.applicationConfigurationService to set
	*/
	public void setApplicationConfigurationService(ApplicationConfigurationService applicationConfigurationService) {
		this.applicationConfigurationService = applicationConfigurationService;
	}

	/**
	* @param paomR003 the this.paomR003 to set
	*/
	public void setPaomR003(PAOMR003 paomR003) {
		this.paomR003 = paomR003;
	}

}