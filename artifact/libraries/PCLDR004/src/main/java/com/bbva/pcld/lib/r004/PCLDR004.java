package com.bbva.pcld.lib.r004;

import com.bbva.pcld.dto.documento.request.ApplicantDTO;
import com.bbva.pcld.dto.documento.request.ProcessInfoDTO;

public interface PCLDR004 {

	void execute(ApplicantDTO applicantDTO, ProcessInfoDTO processInfoDTO);

}
