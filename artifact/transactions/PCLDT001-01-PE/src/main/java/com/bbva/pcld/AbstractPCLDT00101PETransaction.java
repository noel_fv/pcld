package com.bbva.pcld;

import com.bbva.elara.transaction.AbstractTransaction;
import com.bbva.pcld.dto.documento.request.ApplicantDTO;
import com.bbva.pcld.dto.documento.request.ChecklistInfoDTO;
import com.bbva.pcld.dto.documento.request.ProcessInfoDTO;
import com.bbva.pcld.dto.documento.response.DocumentDTO;
import java.util.List;

public abstract class AbstractPCLDT00101PETransaction extends AbstractTransaction {

	public AbstractPCLDT00101PETransaction(){
	}


	/**
	 * Return value for input parameter checklistInfo
	 */
	protected ChecklistInfoDTO getChecklistinfo(){
		return (ChecklistInfoDTO)this.getParameter("checklistInfo");
	}

	/**
	 * Return value for input parameter processInfo
	 */
	protected ProcessInfoDTO getProcessinfo(){
		return (ProcessInfoDTO)this.getParameter("processInfo");
	}

	/**
	 * Return value for input parameter applicant
	 */
	protected ApplicantDTO getApplicant(){
		return (ApplicantDTO)this.getParameter("applicant");
	}

	/**
	 * Set value for List<DocumentDTO> output parameter documents
	 */
	protected void setDocuments(final List<DocumentDTO> field){
		this.addParameter("documents", field);
	}
}
