package com.bbva.pcld;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.pcld.dto.documento.common.FieldDTO;
import com.bbva.pcld.dto.documento.common.ParameterDTO;
import com.bbva.pcld.dto.documento.response.DocumentDTO;
import com.bbva.pcld.lib.r001.PCLDR001;

/**
 * desc
 *
 */
public class PCLDT00101PETransaction extends AbstractPCLDT00101PETransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger("PCLDT00101PETransaction");

	@Override
	public void execute() {
		LOGGER.info("start PCLDT00101PETransaction");
		PCLDR001 pcldR001 = (PCLDR001) getServiceLibrary(PCLDR001.class);
		boolean flag = pcldR001.executeGetDocuments(this.getChecklistinfo(), this.getApplicant(),
				this.getProcessinfo());

		LOGGER.info("[PCLD][PCLDT00101PETransaction] Setting HttpResponseCode 200");
		List<DocumentDTO> listdocuments = new ArrayList<DocumentDTO>();
		DocumentDTO document1 = new DocumentDTO();
		document1.setCode("1");
		document1.setDescription("");
		document1.setId("");
		document1.setName("");
		document1.setRequired(true);
		List<ParameterDTO> parametros = new ArrayList<ParameterDTO>();
		ParameterDTO parameter = new ParameterDTO();
		FieldDTO field = new FieldDTO();
		field.setName("");
		field.setType("");
		parameter.setField(field);
		parameter.setValue("");
		parametros.add(parameter);
		document1.setParameters(parametros);

		DocumentDTO document2 = new DocumentDTO();
		document2.setCode("2");
		document2.setDescription("");
		document2.setId("");
		document2.setName("");
		document2.setRequired(true);
		List<ParameterDTO> parametros2 = new ArrayList<ParameterDTO>();
		ParameterDTO parameter2 = new ParameterDTO();
		FieldDTO field2 = new FieldDTO();
		field2.setName("");
		field2.setType("");
		parameter2.setField(field2);
		parameter2.setValue("");
		parametros2.add(parameter2);
		document2.setParameters(parametros2);

		listdocuments.add(document1);
		listdocuments.add(document2);
		this.setDocuments(listdocuments);
		this.setHttpResponseCode(HttpResponseCode.HTTP_CODE_200);
		this.setSeverity(Severity.OK);

	}

}
